package com.sap.francisco.arias.router;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Map;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Base64;

import com.gigya.socialize.*;

/**
 * Servlet implementation class GigyaExtensionCollector
 */
@WebServlet("/")
public class GigyaExtension extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = LoggerFactory.getLogger(GigyaExtension.class);

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public GigyaExtension() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.getWriter().append("Gigya Extension from SAP Cloud Platform");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {

			doAdd(request, response);
			response.setStatus(200);

		} catch (Exception e) {
			response.getWriter().println("Persistence operation failed with reason: " + e.getMessage());
			LOGGER.error("Persistence operation failed", e);
		}
	}

	private void doAdd(HttpServletRequest request, HttpServletResponse response) throws Exception {
		// Extract name of person to be added from request
		String method = request.getMethod();
		String header = "";
		String parameter = "";
		String body = "-";

		String gigya_sig = "";

		// HEADERS
		Enumeration<String> headerNames = request.getHeaderNames();
		while (headerNames.hasMoreElements()) {
			String headerName = headerNames.nextElement();
			String headerValue = request.getHeader(headerName);

			header += headerName + ":" + headerValue + "\n";

			// Save the Gigya signature
			if (headerName.equals("x-gigya-sig-hmac-sha1")) {
				gigya_sig = headerValue;
			}
		}

		// BODY
		InputStream is = request.getInputStream();
		ByteArrayOutputStream baos = new ByteArrayOutputStream();

		byte buf[] = new byte[1024];
		int letti;

		while ((letti = is.read(buf)) > 0)
			baos.write(buf, 0, letti);

		body = new String(baos.toByteArray());
		// response.getWriter().println("Body: " + body + "\n");

		// PARAMETERS
		Map<String, String[]> params = request.getParameterMap();
		for (Iterator<String> it = params.keySet().iterator(); it.hasNext();) {

			String k = it.next();
			parameter += k + ":";

			String[] v = params.get(k);
			if (v != null && v.length > 0) {
				for (int i = 0; i < v.length; i++) {
					parameter += v[i] + ",";
				}
			}

			parameter += "\n";
		}

		try {

			// EXTENSION
			// ================================

			// Convert the string in JSON object
			JSONParser parser = new JSONParser();
			Object obj = parser.parse(body);
			JSONObject jsonObject = (JSONObject) obj;

			JSONObject data = (JSONObject) jsonObject.get("data");
			JSONObject ev = (JSONObject) data.get("accountInfo");
			String UID = (String) ev.get("UID");

			// USER ALLOWED
			response.setContentType("aplication/json");
			response.setCharacterEncoding("utf-8");
			response.getWriter().print("{\"status\": \"OK\"}");

		} catch (ParseException e) {
			e.printStackTrace();
		}

	}

}
